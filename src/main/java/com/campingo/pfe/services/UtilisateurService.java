package com.campingo.pfe.services;

import java.sql.Date;
import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.campingo.pfe.entities.Utilisateur;

public interface UtilisateurService extends UserDetailsService {
	
	// methodes CRUD Basiques
	public List<Utilisateur> getAllUtilisateurs();
	public Utilisateur findUtilisateurById(Long id);
	public Utilisateur createUtlisateur(Utilisateur utilisateur);
	public Utilisateur updateUtlisateur(Utilisateur utilisateur);
	public void deleteUtlisateur(Long id);
	
	
	// methodes avancees
	public List<Utilisateur> findByFirstName(String firstName);
	public List<Utilisateur> findByFirstNameAndLastName(String firstName, String lastName);

	public List<Utilisateur> findByRolesTitre(String titre);
	
	
	

}
