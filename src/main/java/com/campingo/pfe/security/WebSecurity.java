package com.campingo.pfe.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.campingo.pfe.services.UtilisateurService;


@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UtilisateurService utilisateurService;
	
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return  new BCryptPasswordEncoder();
	}
	
	protected void configure(HttpSecurity http) throws Exception {
		http=http.csrf().disable();
		
		http.authorizeRequests()
		.antMatchers(HttpMethod.POST,"/utilisateur").permitAll()
		.antMatchers(HttpMethod.GET,"/utilisateur").hasAuthority("ADMIN")
		//.antMatchers(HttpMethod.GET,"/utilisateur").hasAnyAuthority("RANDONEUR","USER")
		.anyRequest().authenticated()
		.and().addFilter(new AuthenticationFilter(authenticationManager()))
		.addFilter(new AuthorizationFilter(authenticationManager()))
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	
		http.httpBasic();
		
		
		
		
	}
	
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.userDetailsService(utilisateurService).passwordEncoder(bCryptPasswordEncoder());
		
	}

}
