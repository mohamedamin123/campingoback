package com.campingo.pfe.repostories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.campingo.pfe.entities.Utilisateur;

@Repository
public interface UtilisateurRespository extends JpaRepository<Utilisateur, Long> {
	
	public List<Utilisateur> findByFirstName(String firstName);
	public Utilisateur findByEmail(String email);
	
	
	public List<Utilisateur> findByFirstNameAndLastName(String firstName, String lastName);
	
	
	@Query("SELECT u FROM Utilisateur u WHERE u.firstName = ?1 OR u.lastName = ?2 ")
	public List<Utilisateur> findByFirstNameAndLastNameWithJPQL(String firstName, String lastName);
	
	@Query("SELECT u FROM Utilisateur u WHERE u.firstName LIKE :myFirstName OR u.lastName LIKE :myLastName")
	public List<Utilisateur> findByFirstNameAndLastNameWithJPQLWithNamedParameters(@Param(value = "myFirstName") String firstName,@Param(value = "myLastName") String lastName);
	
	

	

	
	public List<Utilisateur> findByRolesTitre(String titre);
	

}
